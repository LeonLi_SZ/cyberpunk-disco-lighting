def on_on_event():
    global drive_m
    drive_m = difference
    pins.analog_write_pin(AnalogPin.P0,
        Math.constrain(pins.map(drive_m, 0, thres, 100, 1023), 100, 1023))
    basic.pause(randint(10, 20))
    pins.analog_write_pin(AnalogPin.P0, drive_m / 8)
    basic.pause(randint(10, 20))
    control.raise_event(4800, 4801)
control.on_event(4800, 4801, on_on_event)

def on_button_pressed_a():
    global thres
    thres = 40
input.on_button_pressed(Button.A, on_button_pressed_a)

def on_on_event2():
    global drive_b
    drive_b = difference
    pins.analog_write_pin(AnalogPin.P3,
        Math.constrain(pins.map(drive_b, 0, thres, 10, 1023), 10, 1023))
    basic.pause(randint(5, 10))
    pins.analog_write_pin(AnalogPin.P3, drive_b / 8)
    basic.pause(randint(5, 10))
    control.raise_event(4800, 4804)
control.on_event(4800, 4804, on_on_event2)

def on_on_event3():
    global drive_g
    drive_g = difference
    pins.analog_write_pin(AnalogPin.P2,
        Math.constrain(pins.map(drive_g, 0, thres, 10, 1023), 10, 1023))
    basic.pause(randint(5, 10))
    pins.analog_write_pin(AnalogPin.P2, drive_g / 8)
    basic.pause(randint(5, 10))
    control.raise_event(4800, 4803)
control.on_event(4800, 4803, on_on_event3)

def on_on_event4():
    global drive_r
    drive_r = difference
    pins.analog_write_pin(AnalogPin.P1,
        Math.constrain(pins.map(drive_r, 0, thres, 10, 1023), 10, 1023))
    basic.pause(randint(5, 10))
    pins.analog_write_pin(AnalogPin.P1, drive_r / 8)
    basic.pause(randint(5, 10))
    control.raise_event(4800, 4802)
control.on_event(4800, 4802, on_on_event4)

def pwrshow():
    pins.analog_write_pin(AnalogPin.P0, 1023)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P1, 1023)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P1, 0)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P2, 1023)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P2, 0)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P3, 1023)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P3, 0)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P0, 0)
    basic.pause(500)
    pins.analog_write_pin(AnalogPin.P1, 1023)
    pins.analog_write_pin(AnalogPin.P2, 1023)
    pins.analog_write_pin(AnalogPin.P3, 1023)
    basic.pause(500)

def on_button_pressed_ab():
    global thres
    thres = 10
input.on_button_pressed(Button.AB, on_button_pressed_ab)

def on_button_pressed_b():
    global thres
    thres = 60
input.on_button_pressed(Button.B, on_button_pressed_b)

def on_logo_pressed():
    global thres
    thres = 50
input.on_logo_event(TouchButtonEvent.PRESSED, on_logo_pressed)

previous_sound = 0
current_sound = 0
drive_r = 0
drive_g = 0
drive_b = 0
drive_m = 0
thres = 0
difference = 0
led.enable(False)
difference = 0
thres = 20
pwrshow()
pins.analog_write_pin(AnalogPin.P0, 0)
pins.analog_write_pin(AnalogPin.P1, 0)
pins.analog_write_pin(AnalogPin.P2, 0)
pins.analog_write_pin(AnalogPin.P3, 0)
basic.pause(3000)
control.raise_event(4800, 4801)
control.raise_event(4800, 4802)
control.raise_event(4800, 4803)
control.raise_event(4800, 4804)

def on_every_interval():
    global current_sound, difference, previous_sound
    current_sound = input.sound_level()
    difference = abs(current_sound - previous_sound)
    previous_sound = current_sound
loops.every_interval(50, on_every_interval)
